package main

import (
	"fmt"
	"gitlab.com/parapluie/client"
)

func main() {
	user := "anl001"
	role := "admin"
	
	if _, err := client.HasRole(user, role); err == nil {
		fmt.Println("User has role")
	} else {
		fmt.Println("User does not have role")
		fmt.Println(err)
	}
}
