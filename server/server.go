//go:generate protoc -I ../pararpc --go_out=plugins=grpc:../pararpc ../pararpc/pararpc.proto
package main

import (
	"context"
	"log"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	pb "gitlab.com/parapluie/authcheck"
)

const (
	port = ":50051"
)

// Passed to pararpc.RoleChecker for it to be populated
type server struct{}

func (s *server) AuthCheck(ctx context.Context, in *pb.AuthRequest) (*pb.AuthResponse, error) {
	//Returns AuthResponse pointer
	return &pb.AuthResponse{
		Success: true,
		Err:     "",
	}, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	pb.RegisterRoleCheckerServer(grpcServer, &server{})

	reflection.Register(grpcServer)
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to server: %v", err)
	}
}
