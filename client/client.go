package client

import (
	"context"
	"time"
	"fmt"

	"google.golang.org/grpc"
	pb "gitlab.com/parapluie/authcheck"
)
const (
	address = "localhost:50051"
)

//Call the server on AuthCheck service
func HasRole(user string, role string) (bool, error) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		return false, fmt.Errorf("External error: Did not connect: %v", err)
	}
	c := pb.NewRoleCheckerClient(conn)

	//Define context for given call with a timeout of 1 second
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.AuthCheck(ctx, &pb.AuthRequest{
		Name : user,
		Role : role,
	})

	if err != nil {
		return false, fmt.Errorf("External error: Could not AuthCheck: %v", err)
	}

	//Check if server database lookup had failed
	if r.Err != "" {
		return false, fmt.Errorf(r.Err)
	}

	return true, nil
}
