package model

import (
	"gitlab.com/parapluie/model/file"
)

type Model interface {
	GetUser(userId string) User
}

type User struct {
	Id string
	Group []string
	Roles []string
}

type Group struct {
	Id string
	Roles []string
}

type Role struct {
	Id string
	Description string
}

func retrieveUser(userId string, model Model) User {
	u := model.GetUser(userId)
	return u
}

func main() {
	userId := "anl001"
	//Set needed model here
	model := file.NewFileHandler()	

	retrieveUser(userId, model)
}
