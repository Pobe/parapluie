/*
	Save data at specified folder.
	There 3 separates file, user.json, role.json, group.json

	Using files could imply concurrent write. A lock file is used at the given folder.
*/
package file

const (
	user_file = "user.json"
	group_file = "group.json"
	role_file = "role.json"
	lock_file = "parapluie.lock"
	folder = "/tmp/"
)

//File implements the Model model.Model interface
type File struct{}

func (f *File) GetUser(userId string) User {
	return nil 
}

func NewFileHandler() File {
	return File{}
}
